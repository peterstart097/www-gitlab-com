---
layout: markdown_page
title: "Chief of Staff Team READMEs"
---
## Chief of Staff Team READMEs

- [Omar's README (Director of Strategy and Operations)](/handbook/ceo/chief-of-staff-team/readmes/ofernandez2/)
- [Stella's README (Chief of Staff)](/handbook/ceo/chief-of-staff-team/readmes/streas/)
